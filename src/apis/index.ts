import axios from 'axios';
import { Todo } from '../type';
export const getTodos = async (): Promise<Todo[] | Error> => {
  try {
    const result = await axios.get('/todos');
    return result.data;
  } catch (error) {
    return error as Error;
  }
};

export const addTodo = async (data: Todo): Promise<Todo | Error> => {
  try {
    const result = await axios.post('/todos', data);
    return result.data;
  } catch (error) {
    return error as Error;
  }
};
