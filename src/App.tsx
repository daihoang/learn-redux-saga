import React, { useEffect, useRef } from 'react';
import './App.css';
import { useDispatch } from 'react-redux';
import { addToDoRequest, getToDosRequest } from './redux/action';
import { Todo } from './type';
function App() {
  const idRef = useRef<HTMLInputElement | null>(null);
  const userIdRef = useRef<HTMLInputElement | null>(null);
  const titleRef = useRef<HTMLInputElement | null>(null);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getToDosRequest());
  }, []);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const id = idRef.current?.value;
    const userId = userIdRef.current?.value;
    const title = titleRef.current?.value;
    if (id && userId && title) {
      const newData: Todo = {
        id: parseInt(id),
        userId: parseInt(userId),
        title: title,
        completed: false,
      };
      dispatch(addToDoRequest(newData));
    }
  };
  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <input type="number" ref={idRef} />
        <input type="number" ref={userIdRef} />
        <input type="text" ref={titleRef} />
        <button>Submit</button>
      </form>
    </div>
  );
}

export default App;
