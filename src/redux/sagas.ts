import { Todo } from './../type/index';
import { getToDosSuccess, Types, addToDoSuccess } from './action';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import * as api from '../apis';
import { AnyAction } from 'redux';

function* getTodos() {
  try {
    const response: Todo[] = yield call(api.getTodos);
    yield put(getToDosSuccess(response));
  } catch (error) {
    console.log(error);
  }
}
function* addTodos(action: AnyAction) {
  try {
    const response: Todo = yield call(api.addTodo, action.data);
    yield put(addToDoSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

function* watchAddTodo() {
  yield takeEvery(Types.ADD_TODO_REQUEST, addTodos);
}

function* watchGetTodos() {
  yield takeEvery(Types.GET_TODOS_REQUEST, getTodos);
}

export default function* rootSaga() {
  yield all([watchGetTodos(), watchAddTodo()]);
}
