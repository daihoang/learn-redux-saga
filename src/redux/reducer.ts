/* eslint-disable @typescript-eslint/default-param-last */
import { AnyAction, combineReducers } from 'redux';
import { Types } from './action';
import { Todo } from './../type/index';

interface TypeState {
  pending: boolean;
  adding: boolean;
  newTodo?: Todo;
  listTodo: Todo[];
}
const initialState: TypeState = {
  pending: false,
  adding: false,
  listTodo: [
    {
      id: 1,
      userId: 1,
      title: 'Ok',
      completed: false,
    },
  ],
};

const usersReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case Types.GET_TODOS_REQUEST:
      return { ...state, pending: true, listTodo: [] };
    case Types.GET_TODOS_SUCCESS:
      return { ...state, pending: false, listTodo: action.payload };
    case Types.ADD_TODO_REQUEST:
      return { ...state, adding: true };
    case Types.ADD_TODO_SUCCESS:
      return { ...state, adding: false, newTodo: action.payload };
    default:
      return state;
  }
};

export default combineReducers({
  users: usersReducer,
});
