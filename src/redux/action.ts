import { Todo } from '../type';

export const Types = {
  GET_TODOS_REQUEST: 'GET_TODOS_REQUEST',
  GET_TODOS_SUCCESS: 'GET_TODOS_SUCCESS',
  ADD_TODO_REQUEST: 'ADD_TODO_REQUEST',
  ADD_TODO_SUCCESS: 'ADD_TODO_SUCCESS',
};

export const getToDosRequest = () => ({
  type: Types.GET_TODOS_REQUEST,
});

export const getToDosSuccess = (items: Todo[]) => ({
  type: Types.GET_TODOS_SUCCESS,
  payload: items,
});

export const addToDoRequest = (data: Todo) => ({
  type: Types.ADD_TODO_REQUEST,
  data,
});
export const addToDoSuccess = (data: Todo) => ({
  type: Types.ADD_TODO_SUCCESS,
  payload: data,
});
